$(document).ready(function() {

    $(".menu-option").click(function(event) {
        var currentOption = $(this).parents(".selector").find(".name");
        var selectedOption = $(this).find("span");

        var temp = selectedOption.text();

        selectedOption.text(currentOption.text());
        currentOption.text(temp);

        $(this).parents(".menu").toggle("fast");

        $(this).parents(".selector").css("z-index", 1000);

        blu_event("change_ui", "blue");
    });

    $(".toggler").click(function(event) {
        $(this).next(".menu").toggle("fast");

        var selector = $(this).parent();

        if (selector.css("z-index") == 1005) {
            selector.css("z-index", 1000);
        } else {
            selector.css("z-index", 1005);
        }

        $(".selector").each(function() {
            if (!$(this).is(selector) && $(this).css("z-index") == 1005) {
                $(this).children(".menu").toggle("fast");
                $(this).css("z-index", 1000);
            }
        });
    });
});